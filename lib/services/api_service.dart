import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import '../models/author_model.dart';
import '../utils/api_constants.dart';

class ApiService {
  late final Dio _dio;
  ApiService._internal() {
    _dio = Dio();
    if (kDebugMode) {
      _dio.options.baseUrl = 'https://vvf4p681.api.sanity.io/v2021-10-21';
      _dio.interceptors.add(LogInterceptor(
        responseBody: true,
        error: true,
        responseHeader: false,
      ));
    }
  }

  static final ApiService instance = ApiService._internal();

  Future<List<AuthModelData>> getAuthorDetails() async {
    String url = ApiConstants.getAuthorsDetails;

    final response = await _dio.get(url);
    if (response.statusCode == 200) {
      return List.from(response.data['result']).map((value) => AuthModelData.fromJson(value)).toList();
    } else {
      throw Exception("Failed to get author details");
    }
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
