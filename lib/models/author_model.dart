class AuthorModel {
  List<AuthModelData>? result;

  AuthorModel({this.result});

  AuthorModel.fromJson(Map<String, dynamic> json) {
    if (json['result'] != null) {
      result = <AuthModelData>[];
      json['result'].forEach((v) {
        result!.add(AuthModelData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (result != null) {
      data['result'] = result!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AuthModelData {
  String? description;
  String? image;
  String? name;

  AuthModelData({this.description, this.image, this.name});

  AuthModelData.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    image = json['image'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = description;
    data['image'] = image;
    data['name'] = name;
    return data;
  }
}
