import 'package:dew_solutions/models/author_model.dart';
import 'package:dew_solutions/services/api_service.dart';
import 'package:flutter/material.dart';

enum LoadingStatus {
  completed,
  loading,
  empty,
}

class AuthorListViewModel with ChangeNotifier {
  LoadingStatus loadingStatus = LoadingStatus.loading;
  final List<AuthModelData> _authorList = [];
  List<AuthModelData> get authorList => _authorList;
  Future<void> getAuthorDetails() async {
    loadingStatus = LoadingStatus.loading;
    _notify();

    List<AuthModelData> _authorList = await ApiService.instance.getAuthorDetails();

    if (_authorList.isEmpty) {
      loadingStatus = LoadingStatus.empty;
    } else {
      this._authorList.addAll(_authorList);
      loadingStatus = LoadingStatus.completed;
    }
    _notify();
  }

  void _notify(){
    notifyListeners();
  }
}
