import 'package:dew_solutions/screens/author_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../view_models/author_list_view_model.dart';

class AuthorScreen extends StatefulWidget {
  const AuthorScreen({Key? key}) : super(key: key);

  @override
  _AuthorScreenState createState() => _AuthorScreenState();
}

class _AuthorScreenState extends State<AuthorScreen> {
  @override
  void initState() {
    super.initState();

    //get the sanity AuthorList from the API
    WidgetsBinding.instance?.addPostFrameCallback((_) =>
        Provider.of<AuthorListViewModel>(context, listen: false)
            .getAuthorDetails());
  }

  Widget _buildList({AuthorListViewModel? authorListViewModel}) {
    switch (authorListViewModel?.loadingStatus) {
      case LoadingStatus.loading:
        return const Center(
          child: CircularProgressIndicator(),
        );
      case LoadingStatus.completed:
        final authorList = authorListViewModel?.authorList;

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: ListView.builder(
              itemCount: authorList?.length,
              itemBuilder: (BuildContext context, int index) =>
                  AuthorCard(authorModelData: authorList?[index])), //
        );
      case LoadingStatus.empty:
      default:
        return const Center(
          child: Text("No results found"),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Author"),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Consumer(
                  builder: (__, AuthorListViewModel authorList, _) =>
                      _buildList(authorListViewModel: authorList)), //
            ),
          ],
        ),
      ),
    );
  }
}
