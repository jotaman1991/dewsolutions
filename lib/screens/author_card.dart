import 'package:dew_solutions/models/author_model.dart';
import 'package:flutter/material.dart';

class AuthorCard extends StatefulWidget {
  final AuthModelData? authorModelData;
  const AuthorCard({Key? key, this.authorModelData}) : super(key: key);

  @override
  State<AuthorCard> createState() => _AuthorCardState();
}

class _AuthorCardState extends State<AuthorCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  const EdgeInsets.all(10.0),
      child: InkWell(
        onTap: (){
          showDialog(
            context: context,
            builder: (context) {
              return Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                elevation: 16,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height / 2.2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Expanded(flex: 2,
                          child: Row(children: [
                            ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image.network(widget.authorModelData?.image ?? '',
                          frameBuilder: (_,child, frame, wasSynchronouslyLoaded){
                            return frame!=null ? child : const Padding(
                              padding: EdgeInsets.all(20.0),
                              child: CircularProgressIndicator(backgroundColor: Colors.white,valueColor: AlwaysStoppedAnimation<Color>(Colors.blue)),
                            );
                          },),
                      ),
                            Text(
                              widget.authorModelData?.name ?? '',style: const TextStyle(fontWeight: FontWeight.w500,fontSize: 20)),
                          ]),
                        ),
                        Expanded(flex: 9,
                          child: ListView(shrinkWrap: true,
                            children: [
                              Text(widget.authorModelData?.description ?? ''),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          );

        },
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: [
              ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Image.network(
                widget.authorModelData?.image ?? '',
                frameBuilder:
                    (_, child, frame, wasSynchronouslyLoaded) {
                  return frame != null
                      ? child
                      : const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        valueColor:
                        AlwaysStoppedAnimation<Color>(
                            Colors.blue)),
                  );
                },
                width: 100,
                height: 80,
              ),
              ),
              Text(
                  widget.authorModelData?.name ?? '',style: const TextStyle(fontWeight: FontWeight.w500,fontSize: 20),)

            ]),
          ),
        ),
      ),
    );
  }


}
