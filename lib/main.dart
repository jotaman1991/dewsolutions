import 'dart:io';
import 'package:dew_solutions/screens/author_screen.dart';
import 'package:dew_solutions/services/api_service.dart';
import 'package:dew_solutions/view_models/author_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,

        home: MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => AuthorListViewModel())
          ],
          child: const AuthorScreen(),
        ));
  }
}
